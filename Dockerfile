FROM openjdk:8
VOLUME /tmp
EXPOSE 8001
ADD ./target/servicio-cambio-divisas-0.0.1-SNAPSHOT.jar servicio-cambio-divisas.jar
ENTRYPOINT ["java","-jar","/servicio-cambio-divisas.jar"]