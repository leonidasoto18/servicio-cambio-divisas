package com.leosoto.cambio.divisas.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioCambioDivisasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioCambioDivisasApplication.class, args);
	}

}
