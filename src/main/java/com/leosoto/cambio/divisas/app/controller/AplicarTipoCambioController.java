package com.leosoto.cambio.divisas.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leosoto.cambio.divisas.app.dto.ActualizarTipoCambioRequets;
import com.leosoto.cambio.divisas.app.dto.AplicarTipoCambioRequets;
import com.leosoto.cambio.divisas.app.dto.AplicarTipoCambioResponse;
import com.leosoto.cambio.divisas.app.dto.BaseResponse;
import com.leosoto.cambio.divisas.app.service.AplicarTipoCambioService;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@RestController
@RequestMapping(value = "/api/cambioDivisas")
public class AplicarTipoCambioController {

	@Autowired
	private AplicarTipoCambioService aplicarTipoCambioService;

	@PostMapping("/aplicarTipoCambio")
	public Single<ResponseEntity<BaseResponse<AplicarTipoCambioResponse>>> 
	aplicarTipoCambio(@RequestBody AplicarTipoCambioRequets request){
		return aplicarTipoCambioService
				.calcularCambioDivisa(request)
				.subscribeOn(Schedulers.io())
				.map(r -> ResponseEntity.ok(BaseResponse.successWithData(r)));
				
	}
	@PostMapping("/actualizarTipoCambio")
	public Single<ResponseEntity<BaseResponse>> actualizarTipoCambio(@RequestBody ActualizarTipoCambioRequets request){
		return aplicarTipoCambioService.updateTipoCambio(request)
				.subscribeOn(Schedulers.io())
				.toSingle(()->ResponseEntity.ok(BaseResponse.successNoData()));
	}
}
