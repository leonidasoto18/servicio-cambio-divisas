package com.leosoto.cambio.divisas.app.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.leosoto.cambio.divisas.app.entity.TipoCambio;

public interface AplicarTipoCambioDao extends JpaRepository<TipoCambio, Long> {
	public TipoCambio findByFechaTipoCambio(Date fechaTipoCambio);
}
