package com.leosoto.cambio.divisas.app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarTipoCambioRequets {
	private Long id;
	private Double tipoCambio;
}
