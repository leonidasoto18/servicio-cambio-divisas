package com.leosoto.cambio.divisas.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AplicarTipoCambioBase {
	private Double monto;
	private String monedaOrigen;
	private String monedaDestino;	
}
