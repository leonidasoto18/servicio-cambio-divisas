package com.leosoto.cambio.divisas.app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AplicarTipoCambioResponse extends AplicarTipoCambioBase {
	private Double montoConvertido;
	private Double tipoCambio;	
}
