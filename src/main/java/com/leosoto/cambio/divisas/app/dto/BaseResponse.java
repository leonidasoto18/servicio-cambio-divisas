package com.leosoto.cambio.divisas.app.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BaseResponse<T> {
	private String codigoError;
	private String mensaje;
	private T data;

	public static BaseResponse successNoData() {
		return BaseResponse.builder()
				.mensaje("Transaccion Ok")
				.codigoError("000")
				.build();
	}

	public static <T> BaseResponse<T> successWithData(T data) {
		return BaseResponse.<T>builder().data(data)
				.mensaje("Transaccion Ok")
				.codigoError("000")
				.build();
	}

	public static BaseResponse error(String codigoError, String mensaje) {
		return BaseResponse.builder().codigoError(codigoError).mensaje(mensaje).build();
	}
}
