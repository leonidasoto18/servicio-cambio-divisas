package com.leosoto.cambio.divisas.app.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tbl_tipos_de_cambio")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TipoCambio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "tipo_cambio")
	private Double tipoCambio;
	@Column(name = "moneda")
	private String moneda;
	@Column(name = "fecha_tipo_cambio")
	@Temporal(TemporalType.DATE)
	private Date fechaTipoCambio;
	@Column(name = "adicionado_por")
	private String adicionadoPor;
	@Column(name = "fecha_adicion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAdicion;
	@Column(name = "modificado_por")
	private String modificadoPor;
	@Column(name = "fecha_modificacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;
}
