package com.leosoto.cambio.divisas.app.service;

import com.leosoto.cambio.divisas.app.dto.ActualizarTipoCambioRequets;
import com.leosoto.cambio.divisas.app.dto.AplicarTipoCambioRequets;
import com.leosoto.cambio.divisas.app.dto.AplicarTipoCambioResponse;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AplicarTipoCambioService {
	Single<AplicarTipoCambioResponse> calcularCambioDivisa(AplicarTipoCambioRequets aplicarTipoCambioRequets);
    Completable updateTipoCambio(ActualizarTipoCambioRequets actualizarTipoCambioRequets);   
}
