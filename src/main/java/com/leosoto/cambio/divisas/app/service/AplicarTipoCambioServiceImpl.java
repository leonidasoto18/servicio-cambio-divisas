package com.leosoto.cambio.divisas.app.service;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leosoto.cambio.divisas.app.dao.AplicarTipoCambioDao;
import com.leosoto.cambio.divisas.app.dto.ActualizarTipoCambioRequets;
import com.leosoto.cambio.divisas.app.dto.AplicarTipoCambioRequets;
import com.leosoto.cambio.divisas.app.dto.AplicarTipoCambioResponse;
import com.leosoto.cambio.divisas.app.entity.TipoCambio;

import io.reactivex.Completable;
import io.reactivex.Single;

@Service
public class AplicarTipoCambioServiceImpl implements AplicarTipoCambioService {

	@Autowired
	private AplicarTipoCambioDao aplicarTipoCambioDao;

	@Override
	public Single<AplicarTipoCambioResponse> calcularCambioDivisa(AplicarTipoCambioRequets aplicarTipoCambioRequets) {
		return aplicarCambioDivisa(aplicarTipoCambioRequets);
	}

	@Override
	public Completable updateTipoCambio(ActualizarTipoCambioRequets actualizarTipoCambioRequets) {
		return updateTipoCambioEnBase(actualizarTipoCambioRequets);
	}

	private Completable updateTipoCambioEnBase(ActualizarTipoCambioRequets actualizarTipoCambioRequets) {
		return Completable.create(completableSubscriber -> {
			Optional<TipoCambio> optionalTipoCambio = aplicarTipoCambioDao
					.findById(actualizarTipoCambioRequets.getId());
			if (!optionalTipoCambio.isPresent())
				completableSubscriber.onError(new EntityNotFoundException());
			else {
				TipoCambio tipoCambio = optionalTipoCambio.get();
				tipoCambio.setTipoCambio(actualizarTipoCambioRequets.getTipoCambio());
				aplicarTipoCambioDao.save(tipoCambio);
				completableSubscriber.onComplete();
			}
		});
	}

	private Single<AplicarTipoCambioResponse> aplicarCambioDivisa(AplicarTipoCambioRequets request) {
		return Single.create(singleSubscriber -> {
			Optional<TipoCambio> optionalTipoCambio = aplicarTipoCambioDao.findById(1L);
			if (!optionalTipoCambio.isPresent())
				singleSubscriber.onError(new EntityNotFoundException());
			else {
				AplicarTipoCambioResponse response = toAplicarTipoCambioResponse(request);
				response.setTipoCambio(optionalTipoCambio.get().getTipoCambio());
				if (request.getMonedaOrigen().equals("PEN") && request.getMonedaDestino().equals("USD")) {
					response.setMontoConvertido(
							(double) Math.round((request.getMonto() * optionalTipoCambio.get().getTipoCambio())));
				} else if (request.getMonedaOrigen().equals("USD") && request.getMonedaDestino().equals("PEN")) {
					response.setMontoConvertido(
							(double) Math.round((request.getMonto() / optionalTipoCambio.get().getTipoCambio())));
				}
				singleSubscriber.onSuccess(response);
			}
		});
	}

	private AplicarTipoCambioResponse toAplicarTipoCambioResponse(AplicarTipoCambioRequets request) {
		AplicarTipoCambioResponse response = new AplicarTipoCambioResponse();
		BeanUtils.copyProperties(request, response);
		return response;
	}
}
